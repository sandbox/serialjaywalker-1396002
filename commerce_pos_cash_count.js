(function ($) {

/**
 * Automatically update calculated values in the denomination manager.
 */
Drupal.behaviors.commercePosCashCount = {
  attach: function (context, settings) {
    //Utility function for turning entered text into integers.
    var forceInt = function(x) {
      x = parseInt(x);
      if (isNaN(x)) {
        return 0;
      }
      return x;
    };
    
    //Update when values are changed.
    $('.commerce-pos-cash-count-denomination-manager input').change(function() {
      var row = $(this).parents('tr');
      var currencyCode = $('.commerce-pos-cash-count-currency-code', row).val();
      var widget = row.parents('table.commerce-pos-cash-count-denomination-manager');
      //Update the final in this row.
      var initial = forceInt($('.commerce-pos-cash-count-initial input', row).val());
      var deposit = forceInt($('.commerce-pos-cash-count-deposit input', row).val());
      $('.commerce-pos-cash-count-final input', row).val(initial - deposit);

      //Now add up the totals for the currency code that was updated
      var initialTotal = 0;
      var depositTotal = 0;
      $('.commerce-pos-cash-count-denomination-' + currencyCode, widget).each(function() {
        var amount = $('.commerce-pos-cash-count-amount', this).val();
        initialTotal += amount * forceInt($('.commerce-pos-cash-count-initial input', this).val());
        depositTotal += amount * forceInt($('.commerce-pos-cash-count-deposit input', this).val());
      });

      //Send data to the server for formatting and then update
      $.getJSON(settings.basePath + '?q=commerce_pos_cash_count/format_currency/' + currencyCode + '/' + initialTotal + '/' + depositTotal, 
        function(result) {
          $('.commerce-pos-cash-count-total-' + currencyCode + ' .commerce-pos-cash-count-initial').html(result.initial);
          $('.commerce-pos-cash-count-total-' + currencyCode + ' .commerce-pos-cash-count-deposit', widget).html(result.deposit);
          $('.commerce-pos-cash-count-total-' + currencyCode + ' .commerce-pos-cash-count-final', widget).html(result.final);
        });
    });
  }
};

})(jQuery);
