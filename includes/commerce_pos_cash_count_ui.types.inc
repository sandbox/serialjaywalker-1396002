<?php

/**
 * @file
 */


/**
 * Menu callback: display an overview of available types.
 */
function commerce_pos_cash_count_ui_types_overview() {
  drupal_add_css(drupal_get_path('module', 'commerce_pos_cash_count_ui') . '/theme/commerce_pos_cash_count_ui.types.css');

  $header = array(
    t('Name'),
    t('Operations'),
  );

  $rows = array();

  // Loop through all defined cash count types.
  foreach (commerce_pos_cash_count_types() as $type => $cash_count_type) {
    // Build the operation links for the current cash count type.
    $links = menu_contextual_links('commerce-pos-cash-count-type', 'admin/commerce/cash_counts/types', array(strtr($type, array('_' => '-'))));

    // Add the cash count type's row to the table's rows array.
    $rows[] = array(
      theme('commerce_pos_cash_count_type_admin_overview', array('cash_count_type' => $cash_count_type)),
      theme('links', array('links' => $links, 'attributes' => array('class' => 'links inline operations'))),
    );
  }

  // If no cash count types are defined...
  if (empty($rows)) {
    // Add a standard empty row with a link to add a new cash count type.
    $rows[] = array(
      array(
        'data' => t('There are no cash count types yet. <a href="@link">Add cash count type</a>.', array('@link' => url('admin/commerce/cash_counts/types/add'))),
        'colspan' => 2,
      )
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Builds an overview of a cash count type for display to an administrator.
 *
 * @param $variables
 *   An array of variables used to generate the display; by default includes the
 *     type key with a value of the cash count type array.
 *
 * @ingroup themeable
 */
function theme_cash_count_type_admin_overview($variables) {
  $cash_count_type = $variables['cash_count_type'];

  $output = check_plain($cash_count_type['name']);
  $output .= ' <small> (Machine name: ' . check_plain($cash_count_type['type']) . ')</small>';
  $output .= '<div class="description">' . filter_xss_admin($cash_count_type['description']) . '</div>';

  return $output;
}

/**
 * Form callback wrapper: create or edit a cash count type.
 *
 * @param $type
 *   The machine-name of the cash count type being created or edited by this form
 *     or a full cash count type array.
 *
 * @see commerce_pos_cash_count_cash_count_type_form()
 */
function commerce_pos_cash_count_ui_cash_count_type_form_wrapper($type) {
  if (is_array($type)) {
    $cash_count_type = $type;
  }
  else {
    $cash_count_type = commerce_pos_cash_count_type_load($type);
  }

  // Add the breadcrumb for the form's location.
  commerce_pos_cash_count_ui_set_breadcrumb(TRUE);

  // Return a message if the cash count type is not governed by Cash Count UI.
  if (!empty($cash_count_type['type']) && $cash_count_type['module'] != 'commerce_pos_cash_count_ui') {
    return t('This cash count type cannot be edited, because it is not defined by the Cash Count UI module.');
  }

  // Include the forms file from the Cash Count module.
  module_load_include('inc', 'commerce_pos_cash_count_ui', 'includes/commerce_pos_cash_count_ui.forms');

  return drupal_get_form('commerce_pos_cash_count_ui_cash_count_type_form', $cash_count_type);
}

/**
 * Form callback wrapper: confirmation form for deleting a cash count type.
 *
 * @param $type
 *   The machine-name of the cash count type being created or edited by this form
 *     or a full cash count type array.
 *
 * @see commerce_pos_cash_count_cash_count_type_delete_form()
 */
function commerce_pos_cash_count_ui_cash_count_type_delete_form_wrapper($type) {
  if (is_array($type)) {
    $cash_count_type = $type;
  }
  else {
    $cash_count_type = commerce_pos_cash_count_type_load($type);
  }

  // Add the breadcrumb for the form's location.
  commerce_pos_cash_count_ui_set_breadcrumb(TRUE);

  // Return a message if the cash count type is not governed by Cash Count UI.
  if ($cash_count_type['module'] != 'commerce_pos_cash_count_ui') {
    return t('This cash count type cannot be deleted, because it is not defined by the cash count UI module.');
  }

  // Don't allow deletion of cash count types that have cash counts already.
  if (($count = db_query("SELECT cash_count_id FROM {commerce_pos_cash_count} WHERE type = :cash_count_type", array(':cash_count_type' => $cash_count_type['type']))->rowCount()) > 0) {
    drupal_set_title(t('Cannot delete the %name cash count type', array('%name' => $cash_count_type['name'])), PASS_THROUGH);
    return format_plural($count,
      'There is 1 cash count of this type. It cannot be deleted.',
      'There are @count cash counts of this type. It cannot be deleted.'
    );
  }

  // Include the forms file from the cash count module.
  module_load_include('inc', 'commerce_pos_cash_count_ui', 'includes/commerce_pos_cash_count_ui.forms');

  return drupal_get_form('commerce_pos_cash_count_ui_cash_count_type_delete_form', $cash_count_type);
}
