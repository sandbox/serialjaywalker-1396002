<?php

/**
 * Export cash counts to Views.
 */

/**
 * Implements hook_views_data()
 */
function commerce_pos_cash_count_views_data() {
  $data = array();

  $data['commerce_pos_cash_count']['table']['group']  = t('Commerce POS Cash Count');

  $data['commerce_pos_cash_count']['table']['base'] = array(
    'field' => 'count_id',
    'title' => t('Commerce POS Cash Count'),
    'help' => t('Cash counts.'),
    'access query tag' => 'commerce_pos_cash_count_access',
  );

  // Expose the count ID.
  $data['commerce_pos_cash_count']['count_id'] = array(
    'title' => t('Count ID'),
    'help' => t('The unique internal identifier of the count.'),
    'field' => array(
      'handler' => 'commerce_pos_cash_count_handler_field_count',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'commerce_pos_cash_count_handler_argument_count_id',
    ),
  );

  // Expose the count type.
  $data['commerce_pos_cash_count']['type'] = array(
    'title' => t('Type'),
    'help' => t('The human-readable name of the type of the count.'),
    'field' => array(
      'handler' => 'commerce_pos_cash_count_handler_field_count_type',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'commerce_pos_cash_count_handler_filter_count_type',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose the creator uid.
  $data['commerce_pos_cash_count']['uid'] = array(
    'title' => t('Creator'),
    'help' => t('Relate a count to the user who created it.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'field' => 'uid',
      'label' => t('Cash count creator'),
    ),
  );

  // Expose the created and changed timestamps.
  $data['commerce_pos_cash_count']['count_time'] = array(
    'title' => t('Counted date'),
    'help' => t('The date the money was counted.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // Expose the created and changed timestamps.
  $data['commerce_pos_cash_count']['created'] = array(
    'title' => t('Created date'),
    'help' => t('The date the count was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['commerce_pos_cash_count']['changed'] = array(
    'title' => t('Updated date'),
    'help' => t('The date the count was last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // Expose links to operate on the count.
  $data['commerce_pos_cash_count']['view_count'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a simple link to the administrator view of the count.'),
      'handler' => 'commerce_pos_cash_count_handler_field_count_link',
    ),
  );
  $data['commerce_pos_cash_count']['edit_count'] = array(
    'field' => array(
      'title' => t('Edit link'),
      'help' => t('Provide a simple link to edit the count.'),
      'handler' => 'commerce_pos_cash_count_handler_field_count_link_edit',
    ),
  );
  $data['commerce_pos_cash_count']['delete_count'] = array(
    'field' => array(
      'title' => t('Delete link'),
      'help' => t('Provide a simple link to delete the count.'),
      'handler' => 'commerce_pos_cash_count_handler_field_count_link_delete',
    ),
  );

  $data['commerce_pos_cash_count']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all the available operations links for the count.'),
      'handler' => 'commerce_pos_cash_count_handler_field_count_operations',
    ),
  );

  return $data;
}
