<?php

/**
 * @file
 *  Contains default views for the cash count module.
 */

/**
 * Implements hook_views_default_views().
 */
function commerce_pos_cash_count_views_default_views() {
  $view = new view;
  $view->name = 'commerce_pos_cash_counts';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'commerce_pos_cash_count';
  $view->human_name = 'Cash Counts';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Cash Counts';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
  'count_time' => 'count_time',
  'commerce_pos_cash_denominations' => 'commerce_pos_cash_denominations',
  'commerce_pos_cash_denominations_2' => 'commerce_pos_cash_denominations_2',
  'commerce_pos_cash_denominations_1' => 'commerce_pos_cash_denominations_1',
  'operations' => 'operations',
  'count_id' => 'count_id',
  );
  $handler->display->display_options['style_options']['default'] = 'count_time';
  $handler->display->display_options['style_options']['info'] = array(
  'count_time' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
  ),
  'commerce_pos_cash_denominations' => array(
    'align' => '',
    'separator' => '',
  ),
  'commerce_pos_cash_denominations_2' => array(
    'align' => '',
    'separator' => '',
  ),
  'commerce_pos_cash_denominations_1' => array(
    'align' => '',
    'separator' => '',
  ),
  'operations' => array(
    'align' => '',
    'separator' => '',
  ),
  'count_id' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
  ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['order'] = 'desc';
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Field: Commerce POS Cash Count: Counted date */
  $handler->display->display_options['fields']['count_time']['id'] = 'count_time';
  $handler->display->display_options['fields']['count_time']['table'] = 'commerce_pos_cash_count';
  $handler->display->display_options['fields']['count_time']['field'] = 'count_time';
  $handler->display->display_options['fields']['count_time']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['count_time']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['count_time']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['count_time']['alter']['external'] = 0;
  $handler->display->display_options['fields']['count_time']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['count_time']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['count_time']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['count_time']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['count_time']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['count_time']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['count_time']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['count_time']['alter']['html'] = 0;
  $handler->display->display_options['fields']['count_time']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['count_time']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['count_time']['hide_empty'] = 0;
  $handler->display->display_options['fields']['count_time']['empty_zero'] = 0;
  $handler->display->display_options['fields']['count_time']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['count_time']['date_format'] = 'long';
  /* Field: Commerce POS Cash Count: Denominations */
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['id'] = 'commerce_pos_cash_denominations';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['table'] = 'field_data_commerce_pos_cash_denominations';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['field'] = 'commerce_pos_cash_denominations';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['label'] = 'Initial amount';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['alter']['external'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['alter']['html'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['hide_empty'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['empty_zero'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['click_sort_column'] = 'label';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['type'] = 'commerce_pos_cash_denomination_initial';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['group_rows'] = 1;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['delta_offset'] = '0';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['delta_reversed'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations']['field_api_classes'] = 0;
  /* Field: Commerce POS Cash Count: Denominations */
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['id'] = 'commerce_pos_cash_denominations_2';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['table'] = 'field_data_commerce_pos_cash_denominations';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['field'] = 'commerce_pos_cash_denominations';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['label'] = 'Deposit amount';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['alter']['external'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['alter']['html'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['hide_empty'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['empty_zero'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['click_sort_column'] = 'label';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['type'] = 'commerce_pos_cash_denomination_deposit';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['group_rows'] = 1;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['delta_offset'] = '0';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['delta_reversed'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_2']['field_api_classes'] = 0;
  /* Field: Commerce POS Cash Count: Denominations */
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['id'] = 'commerce_pos_cash_denominations_1';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['table'] = 'field_data_commerce_pos_cash_denominations';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['field'] = 'commerce_pos_cash_denominations';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['label'] = 'Final amount';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['alter']['external'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['alter']['html'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['hide_empty'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['empty_zero'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['click_sort_column'] = 'label';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['type'] = 'commerce_pos_cash_denomination_final';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['group_rows'] = 1;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['delta_offset'] = '0';
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['delta_reversed'] = 0;
  $handler->display->display_options['fields']['commerce_pos_cash_denominations_1']['field_api_classes'] = 0;
  /* Field: Commerce POS Cash Count: Operations links */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'commerce_pos_cash_count';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';
  $handler->display->display_options['fields']['operations']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['external'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['operations']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['operations']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['html'] = 0;
  $handler->display->display_options['fields']['operations']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['operations']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['operations']['hide_empty'] = 0;
  $handler->display->display_options['fields']['operations']['empty_zero'] = 0;
  $handler->display->display_options['fields']['operations']['hide_alter_empty'] = 0;
  /* Field: Commerce POS Cash Count: Count ID */
  $handler->display->display_options['fields']['count_id']['id'] = 'count_id';
  $handler->display->display_options['fields']['count_id']['table'] = 'commerce_pos_cash_count';
  $handler->display->display_options['fields']['count_id']['field'] = 'count_id';
  $handler->display->display_options['fields']['count_id']['label'] = '';
  $handler->display->display_options['fields']['count_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['count_id']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['count_id']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['count_id']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['count_id']['alter']['external'] = 0;
  $handler->display->display_options['fields']['count_id']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['count_id']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['count_id']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['count_id']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['count_id']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['count_id']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['count_id']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['count_id']['alter']['html'] = 0;
  $handler->display->display_options['fields']['count_id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['count_id']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['count_id']['hide_empty'] = 0;
  $handler->display->display_options['fields']['count_id']['empty_zero'] = 0;
  $handler->display->display_options['fields']['count_id']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['count_id']['link_to_cash_count'] = 0;
  /* Sort criterion: Commerce POS Cash Count: Counted date */
  $handler->display->display_options['sorts']['count_time']['id'] = 'count_time';
  $handler->display->display_options['sorts']['count_time']['table'] = 'commerce_pos_cash_count';
  $handler->display->display_options['sorts']['count_time']['field'] = 'count_time';
  $handler->display->display_options['sorts']['count_time']['order'] = 'DESC';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/commerce/cash_counts/list';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'List';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['tab_options']['type'] = 'normal';
  $handler->display->display_options['tab_options']['title'] = 'Cash counts';
  $handler->display->display_options['tab_options']['description'] = 'Manage cash counts.';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $handler->display->display_options['tab_options']['name'] = 'management';

  $views['commerce_pos_cash_count_list'] = $view;
  return $views;
}