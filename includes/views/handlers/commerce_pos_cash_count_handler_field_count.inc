<?php

/**
 * @file
 * Contains the basic cash count field handler.
 */

/**
 * Field handler to provide simple renderer that allows linking to a cash count.
 */
class commerce_pos_cash_count_handler_field_count extends views_handler_field {
  function init(&$view, &$options) {
    parent::init($view, $options);

    if (!empty($this->options['link_to_cash_count'])) {
      $this->additional_fields['count_id'] = 'count_id';
    }
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['link_to_cash_count'] = array('default' => FALSE);

    return $options;
  }

  /**
   * Provide the link to cash count option.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['link_to_cash_count'] = array(
      '#title' => t("Link this field to the cash count's administrative view page"),
      '#description' => t('This will override any other link you have set.'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_cash_count']),
    );
  }

  /**
   * Render whatever the data is as a link to the cash count.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($data, $values) {
    if (!empty($this->options['link_to_cash_count']) && $data !== NULL && $data !== '') {
      $count_id = $this->get_value($values, 'count_id');
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = 'admin/commerce/cash_counts/' . $count_id;
    }

    return $data;
  }

  function render($values) {
    $value = $this->get_value($values);
    return $this->render_link($this->sanitize_value($value), $values);
  }
}
