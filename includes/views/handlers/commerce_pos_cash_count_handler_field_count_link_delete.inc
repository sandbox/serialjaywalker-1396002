<?php

/**
 * Field handler to present a link to delete a cash count.
 */
class commerce_pos_cash_count_handler_field_count_link_delete extends commerce_pos_cash_count_handler_field_count_link {
  function construct() {
    parent::construct();

    $this->additional_fields['type'] = 'type';
    $this->additional_fields['uid'] = 'uid';
  }

  function render($values) {
    // Ensure the user has access to delete this cash count.
    $cash_count = commerce_pos_cash_count_new();
    $cash_count->count_id = $this->get_value($values, 'count_id');
    $cash_count->type = $this->get_value($values, 'type');
    $cash_count->uid = $this->get_value($values, 'uid');

    if (!commerce_pos_cash_count_access('delete', $cash_count)) {
      return;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('delete');

    return l($text, 'admin/commerce/cash_counts/' . $vendor->count_id . '/delete', array('query' => drupal_get_destination()));
  }
}
