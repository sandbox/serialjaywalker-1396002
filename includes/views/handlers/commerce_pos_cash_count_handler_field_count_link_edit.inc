<?php

/**
 * Field handler to present a cash count edit link.
 */
class commerce_pos_cash_count_handler_field_cash_count_link_edit extends commerce_pos_cash_count_handler_field_cash_count_link {
  function construct() {
    parent::construct();

    $this->additional_fields['type'] = 'type';
    $this->additional_fields['uid'] = 'uid';
  }

  function render($values) {
    // Ensure the user has access to edit this cash count.
    $cash_count = commerce_pos_cash_count_new();
    $cash_count->count_id = $this->get_value($values, 'count_id');
    $cash_count->type = $this->get_value($values, 'type');
    $cash_count->uid = $this->get_value($values, 'uid');

    if (!commerce_pos_cash_count_access('update', $cash_count)) {
      return;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');

    return l($text, 'admin/commerce/cash_counts/' . $cash_count->count_id . '/edit', array('query' => drupal_get_destination()));
  }
}
