<?php

/**
 * Field handler to translate a cash count type into its readable form.
 */
class commerce_pos_cash_count_handler_field_count_type extends commerce_pos_cash_count_handler_field_count {
  function render($values) {
    $type = $this->get_value($values);
    $value = commerce_pos_cash_count_type_get_name($type);

    return $this->render_link($this->sanitize_value($value), $values);
  }
}
