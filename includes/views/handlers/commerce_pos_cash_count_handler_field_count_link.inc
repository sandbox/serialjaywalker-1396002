<?php

/**
 * Field handler to present a link to a cash count.
 */
class commerce_pos_cash_count_handler_field_count_link extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['count_id'] = 'count_id';
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['text'] = array('default' => '', 'translatable' => TRUE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $text = !empty($this->options['text']) ? $this->options['text'] : t('view');
    $count_id = $this->get_value($values, 'count_id');

    return l($text, 'admin/commerce/cash_counts/' . $count_id);
  }
}
