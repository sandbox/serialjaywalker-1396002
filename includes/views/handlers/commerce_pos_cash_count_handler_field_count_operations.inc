<?php

/**
 * Field handler to present a cash count's operations links.
 */
class commerce_pos_cash_count_handler_field_count_operations extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['count_id'] = 'count_id';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $count_id = $this->get_value($values, 'count_id');

    $links = menu_contextual_links('commerce-pos-cash-count', 'admin/commerce/cash_counts', array($count_id));

    if (!empty($links)) {
      drupal_add_css(drupal_get_path('module', 'commerce_pos_cash_count') . '/theme/commerce_pos_cash_count_views.css');

      return theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline', 'operations'))));
    }
  }
}
