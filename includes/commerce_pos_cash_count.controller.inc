<?php

/**
 * @file
 * The controller for the cash count entity containing the CRUD operations.
 */

/**
 * The controller class for cash counts contains methods for the cash count CRUD operations.
 *
 * Mainly relies on the EntityAPIController class provided by the Entity
 * module, just overrides specific features.
 */
class CommercePosCashCountEntityController extends DrupalCommerceEntityController {

  /**
   * Create a default count.
   *
   * @param array $values
   *   An array of values to set, keyed by property name.
   * @return
   *   A cash count object with all default fields initialized.
   */
  public function create(array $values = array()) {
    return (object) ($values + array(
      'count_id' => '',
      'is_new' => TRUE,
      'uid' => '',
      'type' => 'count',
      'count_time' => time(),
      'created' => '',
      'changed' => '',
    ));
  }

  /**
   * Saves a cash count.
   *
   * @param $count
   *   The full cash count object to save.
   * @param $transaction
   *   An optional database transaction object.
   *
   * @return
   *   The saved count object.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    $transaction = isset($transaction) ? $transaction : db_transaction();

    // Hardcode the changed time.
    $entity->changed = REQUEST_TIME;

    if (empty($entity->{$this->idKey}) || !empty($entity->is_new)) {
      // Set the creation timestamp if not set, for new entities.
      if (empty($entity->created)) {
        $entity->created = REQUEST_TIME;
      }
    }

    return parent::save($entity);
  }

  /**
   * Unserializes the data property of loaded cash counts.
   */
  public function attachLoad(&$queried_counts, $revision_id = FALSE) {
    foreach ($queried_counts as $count_id => &$count) {
      $count->data = unserialize($count->data);
    }

    // Call the default attachLoad() method. This will add fields and call
    // hook_commerce_cash_count_load().
    parent::attachLoad($queried_counts, $revision_id);
  }

  /**
   * Deletes multiple counts by ID.
   *
   * @param $count_ids
   *   An array of count IDs to delete.
   * @param $transaction
   *   An optional transaction object.
   *
   * @return
   *   TRUE on success, FALSE otherwise.
   */
  public function delete($count_ids, DatabaseTransaction $transaction = NULL) {
    if (!empty($count_ids)) {
      $cash_counts = $this->load($count_ids, array());

      // Ensure the counts can actually be deleted.
      foreach ((array) $cash_counts as $count_id => $cash_count) {
        if (in_array(FALSE, module_invoke_all('commerce_pos_cash_count_can_delete', $cash_count))) {
          unset($cash_count[$count_id]);
        }
      }

      // If none of the specified cash counts can be deleted, return FALSE.
      if (empty($cash_counts)) {
        return FALSE;
      }

      parent::delete($count_ids, $transaction);
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
}
