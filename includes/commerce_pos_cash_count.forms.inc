<?php

/**
 * @file
 * Forms for creating, editing, and deleting cash counts.
 */


/**
 * Form callback: create or edit a cash count.
 *
 * @param $cash_count
 *   The cash count object to edit or for a create form an empty cash count object
 *     with only a cash count type defined.
 */
function commerce_pos_cash_count_cash_count_form($form, &$form_state, $cash_count) {
  $language = !empty($cash_count->language) ? $cash_count->language : LANGUAGE_NONE;

  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'commerce_pos_cash_count') . '/includes/commerce_pos_cash_count.forms.inc';

  // Add the field related form elements.
  $form_state['commerce_pos_cash_count'] = $cash_count;
  field_attach_form('commerce_pos_cash_count', $cash_count, $form, $form_state, $language);


  $form['count_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Date and Time'),
    '#description' => t('The date and time at which the money was counted.'),
    '#default_value' => empty($cash_count->count_id) ? commerce_pos_cash_count_date_format(time()) : commerce_pos_cash_count_date_format($cash_count->count_time),
    '#required' => TRUE,
    '#weight' => 35,
  );

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // Simply use default language
  $form['language'] = array(
    '#type' => 'value',
    '#value' => $language,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save cash count'),
    '#submit' => $submit + array('commerce_pos_cash_count_cash_count_form_submit'),
  );

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'commerce_pos_cash_count_cash_count_form_validate';

  return $form;
}

/**
 * Validation callback for commerce_pos_cash_count_cash_count_form().
 */
function commerce_pos_cash_count_cash_count_form_validate($form, &$form_state) {
  $cash_count = $form_state['commerce_pos_cash_count'];
  $cash_count->count_time = strtotime($form_state['values']['count_time']);
  if (empty($cash_count->count_time)) {
    form_set_error('count_time', t('Invalid date/time.'));
  }
  // Notify field widgets to validate their data.
  field_attach_form_validate('commerce_pos_cash_count', $cash_count, $form, $form_state);
}

/**
 * Submit callback for commerce_pos_cash_count_cash_count_form().
 */
function commerce_pos_cash_count_cash_count_form_submit($form, &$form_state) {
  global $user;
  $cash_count = &$form_state['commerce_pos_cash_count'];

  // Save default parameters back into the $cash_count object.
  $cash_count->language = $form_state['values']['language'];
  $cash_count->count_time = strtotime($form_state['values']['count_time']);

  // Set the cash count's uid if it's being created at this time.
  if (empty($cash_count->count_id)) {
    $cash_count->uid = $user->uid;
  }

  // Notify field widgets.
  field_attach_submit('commerce_pos_cash_count', $cash_count, $form, $form_state);
  // Save the count.
  commerce_pos_cash_count_save($cash_count);
  $form_state['redirect'] = 'admin/commerce/cash_counts/' . $cash_count->count_id;

  // Redirect based on the button clicked.
  drupal_set_message(t('Cash count saved.'));
}

/**
 * Form callback: confirmation form for deleting a cash count.
 *
 * @param $cash_count
 *   The cash count object to be deleted.
 *
 * @see confirm_form()
 */
function commerce_pos_cash_count_cash_count_delete_form($form, &$form_state, $cash_count) {
  $form_state['cash_count'] = $cash_count;

  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'commerce_pos_cash_count') . '/includes/commerce_pos_cash_count.forms.inc';

  $form['#submit'][] = 'commerce_pos_cash_count_cash_count_delete_form_submit';

  $content = entity_get_controller('commerce_pos_cash_count')->view(array($cash_count->count_id => $cash_count));

  $form = confirm_form($form,
    t('Are you sure you want to delete the count from %time?', array('%time' => commerce_pos_cash_count_date_format($cash_count->count_time))),
    '',
    drupal_render($content) . '<p>' . t('Deleting this cash count cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for commerce_pos_cash_count_cash_count_delete_form().
 */
function commerce_pos_cash_count_cash_count_delete_form_submit($form, &$form_state) {
  $cash_count = $form_state['cash_count'];

  if (commerce_pos_cash_count_delete($cash_count->count_id)) {
    drupal_set_message(t('The count from %time has been deleted.', array('%time' => commerce_pos_cash_count_date_format($cash_count->count_time))));
    watchdog('commerce_pos_cash_count', 'Deleted cash count from %time.', array('%time' => commerce_pos_cash_count_date_format($cash_count->count_time)), WATCHDOG_NOTICE);
  }
  else {
    drupal_set_message(t('The count from %time could not be deleted.', array('%time' => commerce_pos_cash_count_date_format($cash_count->count_time))), 'error');
  }
}

/**
 * Date formatting function
 */
function commerce_pos_cash_count_date_format($timestamp) {
 return date("F j, Y, g:i a", $timestamp);
}
