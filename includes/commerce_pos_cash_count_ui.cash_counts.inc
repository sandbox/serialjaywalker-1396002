<?php

/**
 * @file
 * Page callbacks and form builder functions for administering cash counts.
 */


/**
 * Menu callback: display a list of cash count types that the user can create.
 */
function commerce_pos_cash_count_ui_add_page() {
  $item = menu_get_item();
  $content = system_admin_menu_block($item);

  // Bypass the admin/commerce/cash_counts/add listing if only one cash count type is
  // available.
  if (count($content) == 1) {
    $item = array_shift($content);
    drupal_goto($item['href']);
  }

  return theme('cash_count_add_list', array('content' => $content));
}

/**
 * Displays the list of available  types for cash count creation.
 *
 * @ingroup themeable
 */
function theme_commerce_pos_cash_count_add_list($variables) {
  $content = $variables['content'];
  $output = '';

  if ($content) {
    $output = '<dl class="commerce-pos-cash-count-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href'], $item['localized_options']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    if (user_access('administer cash count types')) {
      $output = '<p>' . t('You have not installed any  types yet.', array()) . '</p>';
    }
    else {
      $output = '<p>' . t('No cash count types have been created yet for you to use.') . '</p>';
    }
  }

  return $output;
}

/**
 * Form callback wrapper: create or edit a cash count.
 *
 * @param $cash_count
 *   The cash count object being edited by this form.
 *
 * @see commerce_pos_cash_count_cash_count_form()
 */
function commerce_pos_cash_count_ui_cash_count_form_wrapper($cash_count) {
  // Add the breadcrumb for the form's location.
  commerce_pos_cash_count_ui_set_breadcrumb();

  // Include the forms file from the cash count module.
  module_load_include('inc', 'commerce_pos_cash_count', 'includes/commerce_pos_cash_count.forms');

  return drupal_get_form('commerce_pos_cash_count_ui_cash_count_form', $cash_count);
}

/**
 * Form callback wrapper: confirmation form for deleting a cash count.
 *
 * @param $cash_count
 *   The cash count object being deleted by this form.
 *
 * @see commerce_pos_cash_count_cash_count_delete_form()
 */
function commerce_pos_cash_count_ui_cash_count_delete_form_wrapper($cash_count) {
  // Add the breadcrumb for the form's location.
  commerce_pos_cash_count_ui_set_breadcrumb();

  // Include the forms file from the cash count module.
  module_load_include('inc', 'commerce_pos_cash_count', 'includes/commerce_pos_cash_count.forms');

  return drupal_get_form('commerce_pos_cash_count_ui_cash_count_delete_form', $cash_count);
}
